# go.nlx.io

To update [go.nlx.io](https://go.nlx.io/):

1. Make a change to `sally.yaml`
2. Create a Pull Request

When the PR lands, the site will automatically be deployed.